export default interface IInstruction {
  id: number,
  mnemonic: string,
  name: string,
  description: string,
  note: string,
  code: string,
  expertMode: boolean
}