import { TAddressMnemonic } from './TAddressMnemonic';
import { TCategory } from './TCategory';

export default interface IInstruction {
  name: string,
  mnemonic: string,
  category: TCategory,
  description: string,
  note: string | null,
  flags: object,
  addressingModes: TAddressMnemonic[],
  related: string[],
  oposite: string[],
  tier: number
}