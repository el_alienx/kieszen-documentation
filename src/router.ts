// Vue core
import Vue from 'vue'
import Router from 'vue-router'

// Views
import AddressingModes from "./views/AddressingModes.vue"
import Home from './views/Home.vue'
import InstructionSet from './views/InstructionSet.vue'
import Flags from './views/Flags.vue'
import Registers from "./views/Registers.vue"

Vue.use(Router)

export default new Router({
  mode: 'history',
  scrollBehavior: function (to, from, savedPosition) {
    if (to.hash) {
      return { selector: to.hash }
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/addressing-modes',
      name: 'addressing-modes',
      component: AddressingModes
    },
    {
      path: '/instruction-set',
      name: 'instruction-set',
      component: InstructionSet
    },
    {
      path: '/flags',
      name: 'flags',
      component: Flags
    },
    {
      path: '/registers',
      name: 'registers',
      component: Registers
    }
  ]
})
