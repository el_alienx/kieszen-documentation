import IAddressingMode from '@/interfaces/IAddressingMode';

export default abstract class AddressingModesStorage {
  public static readonly addressingModes: IAddressingMode[] = [
    {
      id: 1,
      mnemonic: "IP",
      name: "Implicit",
      description: "For many instructions the source and destination of the information to be manipulated is implied directly by the function of the instruction itself and no further operand needs to be specified. Operations like 'Clear Carry Flag' (CLC) and 'Return from Subroutine' (RTS) are implicit.",
      note: "",
      code: "DEX",
      expertMode: false
    },
    {
      id: 2,
      mnemonic: "IM",
      name: "Inmediate",
      description: "Immediate addressing allows the programmer to directly specify an 8 bit constant within the instruction. It is indicated by a '#' symbol followed by an numeric expression.",
      note: "",
      code: "LDA #44",
      expertMode: false
    },
    {
      id: 3,
      mnemonic: "ZP",
      name: "Zero Page",
      description: "An instruction using zero page addressing mode has only an 8 bit address operand. This limits it to addressing only the first 256 bytes of memory (e.g. $0000 to $00FF) where the most significant byte of the address is always zero. In zero page mode only the least significant byte of the address is held in the instruction making it shorter by one byte (important for space saving) and one less memory fetch during execution (important for speed).",
      note: "",
      code: "STA $01",
      expertMode: false
    },
    {
      id: 4,
      mnemonic: "ZX",
      name: "Zero Page X",
      description: "The address to be accessed by an instruction using indexed zero page addressing is calculated by taking the 8 bit zero page address from the instruction and adding the current value of the X register to it.",
      note: "For example if the X register contains $0F and the instruction LDA $80,X is executed then the accumulator will be loaded from $008F (e.g. $80 + $0F => $8F).",
      code: "LDA $05, X",
      expertMode: false
    },
    {
      id: 5,
      mnemonic: "ZY",
      name: "Zero Page Y",
      description: "The address to be accessed by an instruction using indexed zero page addressing is calculated by taking the 8 bit zero page address from the instruction and adding the current value of the Y register to it.",
      note: "This mode can only be used with the LDX and STX instructions.",
      code: "LDA $05, Y",
      expertMode: false
    },
    {
      id: 6,
      mnemonic: "RE",
      name: "Relative",
      description: "Relative addressing mode is used by branch instructions (e.g. BEQ, BNE, etc.) which contain a signed 8 bit relative offset (e.g. -128 to +127) which is added to program counter if the condition is true. As the program counter itself is incremented during instruction execution by two the effective address range for the target instruction must be with -126 to +129 bytes of the branch.",
      note: "",
      code: "BNE game_over",
      expertMode: true
    },
    {
      id: 7,
      mnemonic: "AB",
      name: "Absolute",
      description: "Instructions using absolute addressing contain a full 16 bit address to identify the target location.",
      note: "",
      code: "STA $0512",
      expertMode: false
    },
    {
      id: 8,
      mnemonic: "AX",
      name: "Absolute X",
      description: "The address to be accessed by an instruction using X register indexed absolute addressing is computed by taking the 16 bit address from the instruction and added the contents of the X register. For example if X contains $92 then an STA $2000,X instruction will store the accumulator at $2092 (e.g. $2000 + $92).",
      note: "",
      code: "STA $0512, X",
      expertMode: false
    },
    {
      id: 9,
      mnemonic: "AY",
      name: "Absolute Y",
      description: "The Y register indexed absolute addressing mode is the same as the previous mode only with the contents of the Y register added to the 16 bit address from the instruction.",
      note: "",
      code: "STA $0512, Y",
      expertMode: false
    },
    {
      id: 10,
      mnemonic: "IA",
      name: "Indirect Absolute",
      description: "JMP is the only 6502 instruction to support indirection. The instruction contains a 16 bit address which identifies the location of the least significant byte of another 16 bit memory address which is the real target of the instruction.\nFor example if location $0120 contains $FC and location $0121 contains $BA then the instruction JMP ($0120) will cause the next instruction execution to occur at $BAFC (e.g. the contents of $0120 and $0121).",
      note: "",
      code: "JMP (high_scores)",
      expertMode: true
    },
    {
      id: 11,
      mnemonic: "IX",
      name: "Indirect X",
      description: "Indexed indirect addressing is normally used in conjunction with a table of address held on zero page. The address of the table is taken from the instruction and the X register added to it (with zero page wrap around) to give the location of the least significant byte of the target address.",
      note: "",
      code: "LDA ($0512, X)",
      expertMode: true
    },
    {
      id: 12,
      mnemonic: "IY",
      name: "Indirect Y",
      description: "Indirect indirect addressing is the most common indirection mode used on the 6502. In instruction contains the zero page location of the least significant byte of 16 bit address. The Y register is dynamically added to this value to generated the actual target address for operation.",
      note: "",
      code: "STA ($0512), Y",
      expertMode: true
    }
  ]
}