import IInstruction from '@/interfaces/IInstruction';

export default abstract class InstructionStorage {
  public static readonly instructions: IInstruction[] = [
    {
      name: "Add with Carry",
      mnemonic: "ADC",
      category: "math",
      description: "Sum the Accumulator plus the Instruction's value plus the Carry flag. If overflow occurs the carry flag turns on, this enables multiple byte addition to being performed.",
      note: "To indicate the processor that you want to do a new add operation, turn off the carry flag using the CLC operand. Otherwise, if the carry flag was turned on by a previous instruction, the processor includes it and will make your math calculus go haywire!",
      flags: {
        carry: "Set if the result was over 255.",
        zero: "Set if the result was 0.",
        overflow: "Set if sign bit is incorrect.",
        negative: "Set if the result can be considered negative."
      },
      addressingModes: ["IM", "ZP", "ZX", "AB", "AX", "AY", "IX", "IY"],
      related: ["CLC"],
      oposite: ["SBC"],
      tier: 0
    },
    {
      name: "Logical AND",
      category: "logic",
      mnemonic: "AND",
      description: "The processor performs a logical AND, bit by bit, testing the accumulator against the instruction value.",
      note: null,
      flags: {
        zero: "Set if the accumulator value is zero",
        negative: "Set if the accumulator result can be considered negative"
      },
      addressingModes: ["IM", "ZP", "ZX", "AB", "AX", "AY", "IX", "IY"],
      related: ["EOR", "ORA", "BIT"],
      oposite: [],
      tier: 2
    },
    {
      name: "Arithmetic Shift Left",
      mnemonic: "ASL",
      category: "bit",
      description: "This instruction shifts all the bits of the Accumulator or the instruction's value one bit to the left. Bit 0 is set to 0 and bit 7 is placed in the Carry flag.",
      note: "The effect of this opcode is to multiply the value by 2 (ignoring 2's complement considerations), setting the carry flag if the result will not fit in 8 bits.",
      flags: {
        carry: "Set to contents of old bit 7",
        zero: "Turns on if the accumulator results in 0",
        negative: "Turns on if the accumulator result can be considered a negative number"
      },
      addressingModes: ["IP", "ZP", "ZX", "AB", "AX"],
      related: ["ROL", "ROR"],
      oposite: ["LSR"],
      tier: 2
    },
    {
      name: "Bit Test",
      mnemonic: "BIT",
      category: "logic",
      description: "This opcode is used to test if one or more bits are set in the instruction value. The mask pattern in the accumulator is ANDed with the instruction value to turn on or off the zero flag, but the result is not kept. Bits 7 and 6 of the value from memory are copied into the negative and overflow flags.",
      note: null,
      flags: {
        zero: "Set if the AND result is 0",
        overflow: "Set to bit 6 of the memory value"
      },
      addressingModes: ["ZP", "AB"],
      related: ["AND"],
      oposite: [],
      tier: 2
    },
    {
      name: "Branch if Carry Clear",
      mnemonic: "BCC",
      category: "branch",
      description: "If the carry flag is off, go to the relative address in the instruction operand.",
      note: null,
      flags: {},
      addressingModes: ["RE"],
      related: ["BEQ", "BPL", "BVC"],
      oposite: ["BCS"],
      tier: 1
    },
    {
      name: "Branch if Carry Set",
      mnemonic: "BCS",
      category: "branch",
      description: "If the carry flag is on, go to the relative address in the instruction operand.",
      note: null,
      flags: {},
      addressingModes: ["RE"],
      related: ["BMI", "BNE", "BVS"],
      oposite: ["BCC"],
      tier: 1
    },
    {
      name: "Branch if Equal",
      mnemonic: "BEQ",
      category: "branch",
      description: "If the zero flag is on, go to the relative address in the instruction operand.",
      note: null,
      flags: {},
      addressingModes: ["RE"],
      related: ["BCS", "BVS", "BPL"],
      oposite: ["BNE"],
      tier: 0
    },
    {
      name: "Branch if Not Equal",
      mnemonic: "BNE",
      category: "branch",
      description: "If the zero flag is off, go to the relative address in the instruction operand.",
      note: null,
      flags: {},
      addressingModes: ["RE"],
      related: ["BCS", "BMI", "BVS"],
      oposite: ["BEQ"],
      tier: 0
    },
    {
      name: "Branch if Positive (+)",
      mnemonic: "BPL",
      category: "branch",
      description: "If the negative flag is off, go to the relative address in the instruction operand.",
      note: null,
      flags: {},
      addressingModes: ["RE"],
      related: ["BCC", "BEQ", "BVC"],
      oposite: ["BMI"],
      tier: 1
    },
    {
      name: "Branch if Minus (-)",
      mnemonic: "BMI",
      category: "branch",
      description: "If the zero flag is on, go to the relative address in the instruction operand.",
      note: null,
      flags: {},
      addressingModes: ["IP"],
      related: ["BCS", "BNE", "BVS"],
      oposite: ["BPL"],
      tier: 1
    },
    {
      name: "Branch if Overflow Clear",
      mnemonic: "BVC",
      category: "branch",
      description: "If the overflow flag is off, go to the relative address in the instruction operand..",
      note: null,
      flags: {},
      addressingModes: ["RE"],
      related: ["BCC", "BEQ", "BPL"],
      oposite: ["BVS"],
      tier: 1
    },
    {
      name: "Branch if Overflow Set",
      mnemonic: "BVS",
      category: "branch",
      description: "If the overflow flag is on, go to the relative address in the instruction operand.",
      note: null,
      flags: {},
      addressingModes: ["RE"],
      related: ["BCS", "BMI", "BNE"],
      oposite: ["BVC"],
      tier: 1
    },
    {
      name: "Break",
      mnemonic: "BRK",
      category: "system",
      description: "This opcode forces the generation of an interrupt request. The program counter and the processor status are pushed to the stack, then the break flag is turn on.",
      note: null,
      flags: {
        break: "Turns on"
      },
      addressingModes: ["IP"],
      related: [],
      oposite: [],
      tier: 0
    },
    {
      name: "Clear Carry Flag",
      mnemonic: "CLC",
      category: "flag",
      description: "Turns off the carry flag.",
      note: "It is used to indicate the processor that you want to do a new add operation.",
      flags: {
        carry: "Turns off"
      },
      addressingModes: ["IP"],
      related: ["CLI", "CLV"],
      oposite: ["SEC"],
      tier: 0
    },
    {
      name: "Clear Interrupt Disable",
      mnemonic: "CLI",
      category: "flag",
      description: "Turns off the interrupt disable flag, allowing normal interrupt request to be handled.",
      note: null,
      flags: {
        interrupt: "Turns off"
      },
      addressingModes: ["IP"],
      related: ["CLC", "CLV"],
      oposite: ["SEI"],
      tier: 2
    },
    {
      name: "Clear Overflow Flag",
      mnemonic: "CLV",
      category: "flag",
      description: "Turns off the overflow flag.",
      note: "This is the only flag instruction that does not have an opposite opcode.",
      flags: {
        overflow: "Turns off"
      },
      addressingModes: ["IP"],
      related: ["CLC", "CLI"],
      oposite: [],
      tier: 1
    },
    {
      name: "Compare",
      mnemonic: "CMP",
      category: "comparisson",
      description: "This opcode compares the contents of the accumulator with the instruction value, and turns on the zero and carry flags as appropriate.",
      note: "How do we know if the numbers are an exact match? Quite simple, behind the scenes, the processor does a subtraction. If the result is zero, thats why the zero flag turns on.",
      flags: {
        carry: "Turns on if the Accumulator is greater or equal than compared the value",
        zero: "Turns on if the Accumulator is equal to the compared value",
        negative: "Turns on is the result is considered negative"
      },
      addressingModes: ["IM", "ZP", "ZX", "AB", "AX", "AY", "IX", "IY"],
      related: ["CPX", "CPY"],
      oposite: [],
      tier: 1
    },
    {
      name: "Compare X Register",
      mnemonic: "CPX",
      category: "comparisson",
      description: "This opcode compares the contents of the X register with the instruction value, and turns on the zero and carry flags as appropriate.",
      note: "See the CPM opcode for more information.",
      flags: {
        carry: "Turns on if the X register is greater or equal than compared the value",
        zero: "Turns on if the X register is equal to the compared value",
        negative: "Turns on is the result is considered negative"
      },
      addressingModes: ["IM", "ZP", "AB"],
      related: ["CPM", "CPY"],
      oposite: [],
      tier: 0
    },
    {
      name: "Compare Y Register",
      mnemonic: "CPY",
      category: "comparisson",
      description: "This opcode compares the contents of the Y register with the instruction value, and turns on the zero and carry flags as appropriate.",
      note: "See the CPM opcode for more information.",
      flags: {
        carry: "Turns on if the Y register is greater or equal than compared the value",
        zero: "Turns on if the Y register is equal to the compared value",
        negative: "Turns on is the result is considered negative"
      },
      addressingModes: ["IM", "ZP", "AB"],
      related: ["CMP", "CPX"],
      oposite: [],
      tier: 0
    },
    {
      name: "Decrement Memory",
      mnemonic: "DEC",
      category: "counter",
      description: "Subtract one from the value held at the specific memory location, turning on or off the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if the result is zero",
        negative: "Turns on if the result is considered negative"
      },
      addressingModes: ["ZP", "ZX", "AB", 'AX'],
      related: ["DEX", "DEY"],
      oposite: ["INC"],
      tier: 1
    },
    {
      name: "Decrement X Register",
      mnemonic: "DEX",
      category: "counter",
      description: "Subtract one from the X register, turning on or off the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if the result is zero",
        negative: "Turns on if the result is considered negative"
      },
      addressingModes: ["IP"],
      related: ["DEC", "DEY"],
      oposite: ["INX",],
      tier: 0
    },
    {
      name: "Decrement Y Register",
      mnemonic: "DEY",
      category: "counter",
      description: "Subtract one from the Y register, turning on or off the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if the result is zero",
        negative: "Turns on if the result is considered negative"
      },
      addressingModes: ["IP"],
      related: ["DEC", "DEX"],
      oposite: ["INY",],
      tier: 0
    },
    {
      name: "Exclusive OR",
      mnemonic: "EOR",
      category: "logic",
      description: "An exclusive OR is performed, bit by bit, on the accumulator against the instruction value.",
      note: null,
      flags: {
        zero: "Set if the result in the Accumulator is zero"
      },
      addressingModes: ["IM", "ZP", "ZX", "AB", "AX", "AY", "IX", "IY"],
      related: ["AND", "ORA"],
      oposite: [],
      tier: 2
    },
    {
      name: "Increment Memory",
      mnemonic: "INC",
      category: "counter",
      description: "Adds one from the value held at the specific memory location, turning on or off the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if the result is zero",
        negative: "Turns on if the result can be considered negative"
      },
      addressingModes: ["ZP", "ZX", "AB", "AX"],
      related: ["INX", "INY"],
      oposite: ["DEC"],
      tier: 1
    },
    {
      name: "Increment X Register",
      mnemonic: "INX",
      category: "counter",
      description: "Adds one from the X register, turning on or off the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if the result is zero",
        negative: "Turns on if the result can be considered negative"
      },
      addressingModes: ["IP"],
      related: ["INC", "INY"],
      oposite: ["DEX"],
      tier: 0
    }, 
    {
      name: "Increment Y Register",
      mnemonic: "INY",
      category: "counter",
      description: "Adds one from the X register, turning on or off the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if the result is zero",
        negative: "Turns on if the result can be considered negative"
      },
      addressingModes: ["IP"],
      related: ["INC", "INX"],
      oposite: ["DEY"],
      tier: 0
    },
    {
      name: "Jump",
      mnemonic: "JMP",
      category: "subroutine",
      description: "Sets the program counter to the address specified by the operand.",
      note: null,
      flags: {},
      addressingModes: ["AB", "IA"],
      related: ["JSR"],
      oposite: [],
      tier: 1
    },
    {
      name: "Jump to Subroutine",
      mnemonic: "JSR",
      category: "subroutine",
      description: "The JSR instruction pushes the address (minus one) of the return point on to the stack and then sets the program counter to the target memory address.",
      note: null,
      flags: {},
      addressingModes: ["AB"],
      related: ["JMP"],
      oposite: ["RTS"],
      tier: 0
    },
    {
      name: "Load Accumulator",
      mnemonic: "LDA",
      category: "memory",
      description: "Loads a byte of memory into the accumulator setting the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if the Accumulator loaded a zero value",
        negative: "Turns on if the Accumulator loaded a number considered negative"
      },
      addressingModes: ["IM", "ZP", "ZX", "AB", "AX", "AY", "IX", "IY"],
      related: ["LDX", "LDY"],
      oposite: ["STA"],
      tier: 0
    },
    {
      name: "Load X Register",
      mnemonic: "LDX",
      category: "memory",
      description: "Loads a byte of memory into the X register setting the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if the Accumulator loaded a zero value",
        negative: "Turns on if the Accumulator loaded a number considered negative"
      },
      addressingModes: ["IM", "ZP", "ZY", "AB", "AY"],
      related: ["LDA", "LDY"],
      oposite: ["STX"],
      tier: 0
    }, 
    {
      name: "Load Y Register",
      mnemonic: "LDY",
      category: "memory",
      description: "Loads a byte of memory into the Y register setting the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if the Accumulator loaded a zero value",
        negative: "Turns on if the Accumulator loaded a number considered negative"
      },
      addressingModes: ["IM", "ZP", "ZX", "AB", "AX"],
      related: ["LDA", "LDX"],
      oposite: ["STY"],
      tier: 0
    },
    {
      name: "Logical Shift Right",
      mnemonic: "LSR",
      category: "memory",
      description: "Each of the bits in the accumulator or the instruction value is shift one place to the right. The bit that was in position 0 is shifted into the carry flag. Bit 7 is set to zero.",
      note: null,
      flags: {
        zero: "Turns on if the Accumulator loaded a zero value",
        negative: "Turns on if the Accumulator loaded a number considered negative"
      },
      addressingModes: ["IP", "ZP", "ZX", "AB", "AX"],
      related: ["ASL"],
      oposite: [],
      tier: 2
    },
    {
      name: "No Operation",
      mnemonic: "NOP",
      category: "system",
      description: "The NOP instruction causes no changes to the processor other than the normal incrementing of the program counter to the next instruction.",
      note: null,
      flags: {},
      addressingModes: ["IP"],
      related: [],
      oposite: [],
      tier: 1
    },
    {
      name: "Logical Inclusive OR",
      mnemonic: "ORA",
      category: "logic",
      description: "An inclusive OR is performed, bit by bit, on the accumulator against the instruction value.",
      note: null,
      flags: {
        zero: "Turns on if the operation result is a zero value",
        negative: "Turns on if the Accumulator loaded a number considered negative"
      },
      addressingModes: ["IM", "ZP", "ZX", "AB", "AX", "AY", "IX", "IY"],
      related: ["AND", "EOR"],
      oposite: [],
      tier: 2
    },
    {
      name: "Push Accumulator",
      mnemonic: "PHA",
      category: "stack",
      description: "Pushes a copy of the accumulator on to the stack.",
      note: null,
      flags: {},
      addressingModes: ["IP"],
      related: ["PHP"],
      oposite: ["PLA"],
      tier: 1
    },
    {
      name: "Push Processor Status",
      mnemonic: "PHP",
      category: "stack",
      description: "Pushes a copy of the status flags on to the stack.",
      note: null,
      flags: {},
      addressingModes: ["IP"],
      related: ["PHA"],
      oposite: ["PLP"],
      tier: 1
    },
    {
      name: "Pull Accumulator",
      mnemonic: "PLA",
      category: "stack",
      description: "Pulls an 8 bit value from the stack and into the accumulator. The zero and negative flags are set as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if all the flags recovered where turned off",
        negative: "Turns on if the recovered value can be considered a negative number"
      },
      addressingModes: ["IP"],
      related: ["PLP"],
      oposite: ["PHA"],
      tier: 1
    },
    {
      name: "Pull Processor Status",
      mnemonic: "PLP",
      category: "stack",
      description: "Move each of the bits in either the accumulator or the instruction value one place to the left. Bit 0 is filled with the current value of the carry whilst the old bit 7 becomes the new carry flag value.",
      note: null,
      flags: {
        carry: "Set from stack",
        zero: "Set from stack",
        overflow: "Set from stack",
        negative: "Set from stack",
        break: "Set from stack",
        interrupt: "Set from stack"
      },
      addressingModes: ["IP"],
      related: ["PLA"],
      oposite: ["PHP"],
      tier: 1
    },
    {
      name: "Rotate Left",
      mnemonic: "ROL",
      category: "bit",
      description: "Move each of the bits in either the accumulator or the instruction value one place to the right. Bit 7 is filled with the current value of the carry flag whilst the old bit 0 becomes the new carry flag value.",
      note: null,
      flags: {
        carry: "Set if there is a carry",
        zero: "Set if the result is zero",
        negative: "Set if the result can be considered negative"
      },
      addressingModes: ["IP", "ZP", "ZX", "AB", "AX"],
      related: ["ASL"],
      oposite: ["ROR"],
      tier: 2
    },
    {
      name: "Rotate Right",
      mnemonic: "ROR",
      category: "bit",
      description: "Move each of the bits in either A or M one place to the right. Bit 7 is filled with the current value of the carry flag whilst the old bit 0 becomes the new carry flag value.",
      note: null,
      flags: {
        carry: "Set if there is a carry",
        zero: "Set if the result is zero",
        negative: "Set if the result can be considered negative"
      },
      addressingModes: ["IP", "ZP", "ZX", "AB", "AX"],
      related: ["ASR"],
      oposite: ["ROL"],
      tier: 2
    },
    {
      name: "Return from Interrupt",
      mnemonic: "RTI",
      category: "system",
      description: "The RTI instruction is used at the end of an interrupt processing routine. It pulls the processor flags from the stack followed by the program counter.",
      note: null,
      flags: {
        carry: "Set from stack",
        zero: "Set from stack",
        overflow: "Set from stack",
        negative: "Set from stack",
        break: "Set from stack",
        interrupt: "Set from stack"
      },
      addressingModes: ["IP"],
      related: [],
      oposite: [],
      tier: 2
    },
    {
      name: "Return from Subroutine",
      mnemonic: "RTS",
      category: "subroutine",
      description: "The RTS instruction is used at the end of a subroutine to return to the calling routine. It pulls the program counter (minus one) from the stack.",
      note: null,
      flags: {},
      addressingModes: ["IP"],
      related: [],
      oposite: ["JSR"],
      tier: 0
    },
    {
      name: "Substract with Carry",
      mnemonic: "SBC",
      category: "math",
      description: "Subtract the accumulator with the instruction value taking into account the carry flag. If overflow occurs the carry flag turns off, this enables multiple byte addition to be performed.",
      note: "To indicate the processor that you want to do a new subtract operation, turn on the carry flag using the SEC operand. Otherwise if the carry flag was turned off by a previous instruction, the processor forgets to include it and will make your math calculus go haywire!",
      flags: {
        carry: "Set if the result is under 0",
        overflow: "Set if sign bit is incorrect",
        zero: "Set if the result was 0",
        negative: "Set if the result can be considered negative"
      },
      addressingModes: ["IM", "ZP", "ZX", "AB", "AX", "AY", "IX", "IY"],
      related: ["SEC"],
      oposite: ["ADC"],
      tier: 0
    },
    {
      name: "Set Carry Flag",
      mnemonic: "SEC",
      category: "flag",
      description: "Turns on the carry flag.",
      note: "It is used to indicate the processor that you want to do a new subtract operation.",
      flags: {
        carry: "Turns on"
      },
      addressingModes: ["IP"],
      related: ["SEI"],
      oposite: ["CLC"],
      tier: 0
    },
    {
      name: "Set Interrupt Disable",
      mnemonic: "SEI",
      category: "flag",
      description: "Turns on the interrupt disable flag, blocking any interrupt request received.",
      note: null,
      flags: {
        interrupt: "Turns on"
      },
      addressingModes: ["IP"],
      related: ["SEC"],
      oposite: ["CLI"],
      tier: 2
    },
    {
      name: "Store Accumulator",
      mnemonic: "STA",
      category: "memory",
      description: "Stores the contents of the accumulator into memory.",
      note: null,
      flags: {},
      addressingModes: ["IM", "ZP", "ZX", "AB", "AX", "AY", "IX", "IY"],
      related: ["STX", "STY"],
      oposite: ["LDA"],
      tier: 0
    },
    {
      name: "Store X Register",
      mnemonic: "STX",
      category: "memory",
      description: "Stores the contents of the X register into memory.",
      note: null,
      flags: {},
      addressingModes: ["ZP", "ZY", "AB"],
      related: ["STA", "STX"],
      oposite: ["LDX"],
      tier: 0
    },
    {
      name: "Store Y Register",
      mnemonic: "STY",
      category: "memory",
      description: "Stores the contents of the Y register into memory.",
      note: null,
      flags: {},
      addressingModes: ["ZP", "ZX", "AB"],
      related: ["STA", "STX"],
      oposite: ["LDY"],
      tier: 0
    },
    {
      name: "Transfer Accumulator to X",
      mnemonic: "TAX",
      category: "register",
      description: "Copies the current contents of the accumulator into the X register and sets the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if X equals to 0",
        negative: "Turns on if X can be considered a negative number"
      },
      addressingModes: ["IP"],
      related: ["TAY"],
      oposite: ["TXA"],
      tier: 1
    },
    {
      name: "Transfer Accumulator to Y",
      mnemonic: "TAY",
      category: "register",
      description: "Copies the current contents of the accumulator into the Y register and sets the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if Y equals to 0",
        negative: "Turns on if Y can be considered a negative number"
      },
      addressingModes: ["IP"],
      related: ["TAX"],
      oposite: ["TYA"],
      tier: 1
    },
    {
      name: "Transfer Stack Pointer to X",
      mnemonic: "TSX",
      category: "register",
      description: "Copies the current contents of the stack register into the X register and sets the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if X equals to 0",
        negative: "Turns on if X can be considered a negative number"
      },
      addressingModes: ["IP"],
      related: [],
      oposite: ["TXS"],
      tier: 1
    },
    {
      name: "Transfer X to Accumulator",
      mnemonic: "TXA",
      category: "register",
      description: "Copies the current contents of the X register into the accumulator and sets the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if A equals to 0",
        negative: "Turns on if A can be considered a negative number"
      },
      addressingModes: ["IP"],
      related: [""],
      oposite: ["TAX"],
      tier: 1
    },
    {
      name: "Transfer X to Stack Pointer",
      mnemonic: "TXS",
      category: "register",
      note: null,
      description: "Copies the current contents of the X register into the stack register.",
      flags: {},
      addressingModes: ["IP"],
      related: [""],
      oposite: ["TSX"],
      tier: 1
    },
    {
      name: "Transfer Y to Accumulator",
      mnemonic: "TYA",
      category: "register",
      description: "Copies the current contents of the Y register into the accumulator and sets the zero and negative flags as appropriate.",
      note: null,
      flags: {
        zero: "Turns on if A equals to 0",
        negative: "Turns on if A can be considered a negative number"
      },
      addressingModes: ["IP"],
      related: ["TXA"],
      oposite: ["TAY"],
      tier: 1
    }
  ]
}